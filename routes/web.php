<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// --------> ROUTE ACCUEIL
Route::view('/','home');

//--------> PAGE AFICHER ZONE
Route::get('/zone','ZoneController@show');

// -------> PAGE AJOUTER UNE NOUVELLE ZONE
Route::get('/report','ZoneController@select');
Route::post('/reportZone','ZoneController@addZone');

//--------> PAGE AJOUTER CAILLOUX
Route::post('/add','RockController@addCailloux');

//--------> PAGE LIST CAILLOUX
Route::get('/rocks', 'RockController@show');


