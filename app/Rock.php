<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rock extends Model
{
    protected $fillable =[
        'name',
        'level',
        'info'
    ];
}
