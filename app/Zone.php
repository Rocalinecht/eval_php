<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Zone extends Model
{
    protected $fillable =[
        'name_zone',
        'name_rock',
        'level_danger',
        'place_zone'
    ];
}
