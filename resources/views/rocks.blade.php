@extends('layout')
       
@section('content')
<div class="jumbotron jumbotron-fluid">
    <div class="container">
        <h1 class="display-4 text-center">Liste des cailloux dangereux </h1>
    </div>
</div>
 

<table class="table w-75 mx-auto">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Nom</th>
            <th scope="col">Niveau</th>
            <th scope="col">Info</th>
          </tr>
        </thead>
        <tbody>
                @foreach ($cailloux as $rock)
          <tr>
            <th scope="row">1</th>
            <td>{{ $rock->name }}</td>
            <td>{{ $rock->level }}</td>
            <td>{{ $rock->info }}</td>
          </tr>
          @endforeach
        </tbody>
      </table>
@endsection