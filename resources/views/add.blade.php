@extends('layout')

@section('content')
<div class="jumbotron jumbotron-fluid">
    <div class="container">
        <h1 class="display-4 text-center">Ajouter un nouveau danger</h1>
    </div>
</div>
<div class="mx-auto w-75">
<form action="/add" method="POST">
    @csrf
    <div class="form-group">
      <label for="name">Nom du cailloux : </label>
      <input type="text" name='name' class="form-control" id="name" placeholder="name rock">
    </div>
    <div class="form-group">
      <label for="level_danger">Niveau de danger :</label>
      <select class="form-control" name='level_danger'  id="level_danger">
        <option value="1">1 (faible) </option>
        <option value="2">2 (Moyenne) </option>
        <option value="3">3 (Grave) </option>
        <option value="4">4 (Très grave) </option>
      </select>
    </div>
    <div class="form-group">
      <label for="info">Informations : </label>
      <textarea class="form-control" name="info" id="info" rows="3"></textarea>
    </div>
    <button type="submit" class="btn btn-primary mb-2">Ajouter</button>
  </form>
</div>

@endsection
