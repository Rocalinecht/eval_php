@extends('layout')

@section('content')
<div class="jumbotron jumbotron-fluid">
    <div class="container">
        <h1 class="display-4 text-center">Signaler une nouvelle zone</h1>
    </div>
</div>
<div class="mx-auto w-75">
    <form action="/reportZone" method="POST">
    @csrf
        <div class="form-group">
            <label for="name">Nom de la nouvelle zone : </label>
            <input type="text" name='name' class="form-control" id="name" placeholder="name zone">
        </div>
        <div class="form-group">
            <label for="name_rock">Cailloux present dans la zone:</label>
            <select class="form-control" name='name_rock'  id="name_rock">
            @foreach ($nomcailloux as $nom)
                <option value="{{$nom->name_rock}}">{{$nom->name_rock}}</option>
            @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="level_dangerZone">Niveau de danger :</label>
            <select class="form-control" name='level_dangerZone'  id="level_dangerZone" >
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3 </option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
                <option value="7">7</option>
                <option value="8">8</option>
                <option value="9">9</option>
                <option value="10">10</option>
            </select>
        </div>
        <div class="form-group">
            <label for="place">Localisation : </label>
            <input type="text" name='place' class="form-control" id="place" placeholder="X:000° Y:000°">
        </div>
        <button type="submit" class="btn btn-primary mb-2">Ajouter</button>
    </form>
</div>
@endsection
