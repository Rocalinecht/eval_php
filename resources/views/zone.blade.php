
@extends('layout')
@section('content')
<div class="jumbotron jumbotron-fluid">
  <div class="container text-center">
    <h1 class="display-4 text-center">Liste zone dangereuse</h1><br>
  </div>
</div>
<div class="mx-auto d-flex w-75  "><br><br>
  <img src="/img/atlas_hemi1.gif"  class="rounded mx-auto d-block" alt="">
</div><br><br>      
<table class="table w-75 mx-auto">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nom de la zone</th>
      <th scope="col">Cailloux présent</th>
      <th scope="col">Niveau Danger </th>
      <th scope="col">Coodonnée</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($carte as $zone)
    <tr>
      <th scope="row">1</th>
      <td>{{ $zone->name_zone }}</td>
      <td>{{ $zone->name_rock }}</td>
      <td>{{ $zone->level_danger }}</td>
      <td>{{ $zone->place_zone}}</td>
    </tr>
    @endforeach
  </tbody>
</table>          
@endsection       