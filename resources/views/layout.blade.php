<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title> LCM Laravel</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    </head>
    <body >
        <nav class="navbar navbar-light  justify-content-between">
            <a href="/" class="navbar-brand text-muted">Let's colonize Mars !</a>
            <form class="form-inline">
                <a href="/rocks" class="mr-3 text-muted"> Liste Cailloux </a>
                <a href="/zone" class="mr-3 text-muted"> Liste Zone </a>
                <a href="/report" class="mr-3 "> Signaler une Zone </a>
                <a href="/add"  class="mr-3 "> Ajouter un cailloux</a>    
            </form>
        </nav>
        <main class="py-4">
            @yield('content')
        </main>
    </body>
</html>
                          