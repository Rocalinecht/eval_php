@extends('layout')
       
@section('content')
<div class="jumbotron jumbotron-fluid">
    <div class="container text-center">
        <h1 class="display-4 text-center">Welcome les Martiens !</h1><br>
        <p>Cette planete est dangereuse ... Alors aidez nous à identifier les zones à risques</p>
        <p>Et pour les plus courageux d'entre vous, faite nous part de vos découvertes sur de nouveaux cailloux </p>
    </div>
</div>
<div class=" text-center">
  <div>
    <button type="button" class="btn btn-primary "><a href="/rocks" class="mr-3 text-white"> Liste Cailloux</a></button>
    <button type="button" class="btn btn-primary "><a href="/zone" class="mr-3 text-white"> Liste Zone </a></button>
    <button type="button" class="btn btn-primary "><a href="/report" class="mr-3 text-white"> Signaler une Zone </a></button>
    <button type="button" class="btn btn-primary "><a href="/add"  class="mr-3 text-white"> Ajouter un cailloux</a></button>    
  </div>
</div>
<div class="mx-auto  ">
   <br><br>
    <img src="/img/atlas_hemi1.gif"  class="rounded mx-auto d-block" alt="">
</div>
@endsection