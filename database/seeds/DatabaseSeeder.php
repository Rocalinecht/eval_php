<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('rocks')->insert([
            'name' => 'Klingon',
            'level' => '2',
            'info' => 'un peu relou, altère les combinaisons, traverse les matériaux et provoque des démangeaisons cutanées,  On a observé une résistance chez les geeks ',
        ]);
        DB::table('rocks')->insert([
            'name' => 'Chomdû',
            'level' => '3',
            'info' => 'minerai qui provoque des états dépressifs. Bizzarement, les personnes âgées ne sont plus affectées... ',
        ]);
        DB::table('rocks')->insert([
            'name' => 'Perl',
            'level' => '4',
            'info' => 'minerai à éviter à tout prix ! Hystérie, dépression, folie, voir mort subite sont les effets fréquemment observés.',
        ]);
        DB::table('zones')->insert([
            'name_zone' => 'Acidalia Planitia',
            'name_rock'=> 'Chomdû',
            'level_danger' => '8',
            'place_zone' => 'X:30°-Y:50°',
            
        ]);
        DB::table('zones')->insert([
            'name_zone' => 'Amazonis Planitia',
            'name_rock'=> 'Perl',
            'level_danger' => '3',
            'place_zone' => 'X:170°-Y:20°',
            
        ]);
        DB::table('zones')->insert([
            'name_zone' => 'Luna Planum',
            'name_rock'=> ' Klingon',
            'level_danger' => '10',
            'place_zone' => 'X:70°-Y:10°',
            
        ]);
    }
}
